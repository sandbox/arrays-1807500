This Drupal 7 module allows to update the node comments statistics using the Youtube API.

It is set to run on cron. 
It retrieves the comment count thread and update the database with the count. This module is necessary if you want to user comment count in sorting in views but you are not storing comments in your database.

Installation

Install the module as you do any other module in drupal. For this module to work, you must have Zend liberary installed on your server.

For further details on Zend liberary install, read here https://developers.google.com/youtube/2.0/developers_guide_php#Getting_Started




